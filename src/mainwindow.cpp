#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QRgb>
#include <QDebug>
#include <colordatabasemanagement.h>
#include <QPainter>
#include <QSettings>


/**
 * MainWindow constructor sets up the gui
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->visualColor->setAutoFillBackground(true);
    ui->visualColor_2->setAutoFillBackground(true);
    ui->visualAcrylic->setAutoFillBackground(true);
    ui->label->setCursor(Qt::CrossCursor);

    //setup cmyk chart
    QVector<QColor> colors;
    colors << Qt::cyan << Qt::magenta << Qt::yellow << Qt::black;
    ui->columnChart->setColumnColors(colors);

    allColorsDB = new ColorNameDatabase(ColorNameDatabase::DATABASE_ALLCOLORS);
    acrylicColorsDB = new ColorNameDatabase(ColorNameDatabase::DATABASE_ACRYLIC_COLORS);

    mouseRightButtonPressed = false;
}

/**
 * @brief MainWindow::getOriginalXYPixelPosition
 * @param pos position relative to label
 */
QPoint MainWindow::getOriginalXYPixelPosition(QPoint pos) {
    //position relative to the pixmap
    pos -= QPoint((ui->label->width() - ui->label->pixmap()->width())/2,
                  (ui->label->height() - ui->label->pixmap()->height())/2);

    //correct with scale
    qreal scale = (qreal)ui->label->pixmap()->height()/(qreal)image.height();
    pos.setX( (qreal)pos.x()/scale );
    pos.setY( (qreal)pos.y()/scale );
    return pos;
}

/**
 * Capture mouse position
 * If the position is ok, then it set the position and color info labels
 * If the right mouse button is pressed, it fills the Grid info and draws it
 * @param event
 */
void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if(ui->label->pixmap() != NULL) {
        QPoint pos = getOriginalXYPixelPosition (
                    ui->label->mapFromGlobal(event->globalPos()) );
        setInformationLabels(pos);

        if(mouseRightButtonPressed) {
            ui->grid_width->setText(QString().setNum(pos.x() - ui->grid_x->text().toInt() ));
            ui->grid_height->setText(QString().setNum(pos.y() - ui->grid_y->text().toInt() ));
            //this is slow, I should use an overlay drawing
            drawAndScaleImage(1.0);
        }
    }
    QWidget::mouseMoveEvent(event);
}

/**
 * Put the Grid topleft point
 * @param e
 */
void MainWindow::mousePressEvent(QMouseEvent *e) {
    if(ui->label->pixmap() != NULL && e->button() == Qt::RightButton) {
        QPoint pos = getOriginalXYPixelPosition (
                    ui->label->mapFromGlobal(e->globalPos()) );
        ui->grid_x->setText(QString().setNum(pos.x()));
        ui->grid_y->setText(QString().setNum(pos.y()));
        mouseRightButtonPressed = true;
    }
    QWidget::mousePressEvent(e);
}

/**
 * Sets the mouseRightButtonPressed to false;
 * @param e
 */
void MainWindow::mouseReleaseEvent(QMouseEvent *e) {
    if(ui->label->pixmap() != NULL) {
        mouseRightButtonPressed = false;
    }
    QWidget::mouseReleaseEvent(e);
}

/**
 * Handles key events
 * Plus, minus buttons scale the image
 * @param event
 */
void MainWindow::keyPressEvent(QKeyEvent *event) {
    if(ui->label->pixmap() == NULL) {
        event->ignore();
        return;
    }

    if(event->key() == Qt::Key_P) { //plus
        drawAndScaleImage(10.0/7.0);
    }
    else if(event->key() == Qt::Key_M) { //minus
        drawAndScaleImage(0.7);
    }

    QWidget::keyPressEvent(event);
}


/**
 * User opens an image file and sets to the label
 */
void MainWindow::openImage() {
    QSettings settings("paintmixer.settings", QSettings::IniFormat);
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Image"),
                                                    settings.value("lastOpenPath", "").toString(),
                                                    tr("Image Files (*.png *.jpg *.bmp *.gif)"));

    if(fileName.isNull() || !QFile::exists(fileName)) { //User cancelled the fileOpen
        return;
    }
    settings.setValue("lastOpenPath", QFileInfo(fileName).absoluteDir().absolutePath());

    imageFileName = fileName;
    image.load(fileName);

    if(image.isNull() ) {
        QMessageBox::warning(this, tr("Paint Mixer"),
                             tr("Could not open image file."),
                             QMessageBox::Ok);
        return;
    }

    ui->label->setPixmap(
                QPixmap::fromImage(
                    image.scaledToHeight(ui->scrollArea->height())
                    )
                );

    loadInformationPage();
}

/**
 * When user closes the window, it saves the information page
 * @param event
 */
void MainWindow::closeEvent(QCloseEvent *event) {
    saveInformationPage();
    QWidget::closeEvent(event);
}

/**
 * Load information page data from a file named image_file_name.paintmixer
 */
void MainWindow::loadInformationPage() {
    if(imageFileName != "") {
        QSettings settings(imageFileName+".paintmixer", QSettings::IniFormat);
        ui->orig_size_x->setText(QString().setNum(settings.value("origWidth", 0).toDouble()));
        ui->orig_size_y->setText(QString().setNum(settings.value("origHeight", 0).toDouble()));

        ui->grid_x->setText(QString().setNum(settings.value("gridX", 0).toInt()));
        ui->grid_y->setText(QString().setNum(settings.value("gridY", 0).toInt()));
        ui->grid_width->setText(QString().setNum(settings.value("gridWidth", 0).toInt()));
        ui->grid_height->setText(QString().setNum(settings.value("gridHeight", 0).toInt()));
        ui->grid_cellsize->setText(QString().setNum(settings.value("gridCellSize", 0).toDouble()));

        ui->gaussianMatrixSize->setValue(settings.value("gaussianMatrixSize", 0).toInt());
    }
}

/**
 * Save information page data to a file named image_file_name.paintmixer
 */
void MainWindow::saveInformationPage() {
    if(imageFileName != "") {
        QSettings settings(imageFileName+".paintmixer", QSettings::IniFormat);

        settings.setValue("origWidth", ui->orig_size_x->text().toDouble());
        settings.setValue("origHeight", ui->orig_size_y->text().toDouble());

        settings.setValue("gridX", ui->grid_x->text().toInt());
        settings.setValue("gridY", ui->grid_y->text().toInt());
        settings.setValue("gridWidth", ui->grid_width->text().toInt());
        settings.setValue("gridHeight", ui->grid_height->text().toInt());
        settings.setValue("gridCellSize", ui->grid_cellsize->text().toDouble());

        settings.setValue("gaussianMatrixSize", ui->gaussianMatrixSize->value());
    }
}

/**
 * Destructor of the MainWindow
 */
MainWindow::~MainWindow() {
    delete ui;
}


/**
 * Set position and color information labels
 * according to a position in the image
 * @param pos a point in the image
 */
void MainWindow::setInformationLabels(QPoint &pos) {
    if(!image.isNull() && pos.x() >= 0 && pos.y() >= 0
            && pos.x() < image.width() && pos.y() < image.height()) {
        //set x, y label
        ui->x_pos->setText( QString().setNum( pos.x() ));
        ui->y_pos->setText( QString().setNum( pos.y() ));

        //set RGB labels
        QRgb pixel = image.pixel(pos.x(), pos.y());

        if(ui->gaussianMatrixSize->value() > 1) {
            //calculate blurred value for one pixel
            pixel = blur.calculateOnePixel(pos.x(), pos.y(), &image);
        }

        ui->r_color->setText( QString().setNum( qRed(pixel) ));
        ui->g_color->setText( QString().setNum( qGreen(pixel) ));
        ui->b_color->setText( QString().setNum( qBlue(pixel) ));

        //set visual-label color
        QPalette p(ui->visualColor->palette());
        p.setColor(ui->visualColor->backgroundRole(),
                   QColor(pixel));
        ui->visualColor->setPalette(p);

        //set name label, acrylic name label
        NamedColor nc = allColorsDB->nearestColorName(qRed(pixel),
                                                      qGreen(pixel),
                                                      qBlue(pixel));

        NamedColor acrylicName = acrylicColorsDB->nearestColorName(qRed(pixel),
                                                              qGreen(pixel),
                                                              qBlue(pixel));
        ui->colorNameLabel->setText(nc.name);
        ui->acrylicName->setText(acrylicName.name);

        //matched color visually, and rgb
        ui->matched_r->setText(QString().setNum(nc.r));
        ui->matched_g->setText(QString().setNum(nc.g));
        ui->matched_b->setText(QString().setNum(nc.b));

        // Set matched color visual label
        QPalette p2(ui->visualColor_2->palette());
        p2.setColor(ui->visualColor_2->backgroundRole(),
                    QColor((int)nc.r, (int)nc.g, (int)nc.b));
        ui->visualColor_2->setPalette(p2);

        //set acrylic visual-label color
        QPalette p3(ui->visualColor->palette());
        p3.setColor(ui->visualColor->backgroundRole(),
                   QColor((int)acrylicName.r, (int)acrylicName.g, (int)acrylicName.b));
        ui->visualAcrylic->setPalette(p3);

        //set positions according to original image size
        double origSizeX = ui->orig_size_x->text().toDouble();
        double origSizeY = ui->orig_size_y->text().toDouble();
        ui->orig_pos_x->setText(
                    QString().setNum( ((double)pos.x()/(double)image.width()) * origSizeX));
        ui->orig_pos_y->setText(
                    QString().setNum( ((double)pos.y()/(double)image.height()) * origSizeY));

        //set cmyk chart
        QColor col(pixel);
        int c, m, y, k, a;
        col.getCmyk(&c, &m, &y, &k, &a);

        ui->columnChart->setColumnPercentage(0, ((double)c/255.0));
        ui->columnChart->setColumnPercentage(1, ((double)m/255.0));
        ui->columnChart->setColumnPercentage(2, ((double)y/255.0));
        ui->columnChart->setColumnPercentage(3, ((double)k/255.0));
    }
}


/**
 * Creates a new Gaussian blur object with kernel size
 * @param value will be the kernel size
 */
void MainWindow::generateGaussianMatrix(int value) {
    blur = GaussianBlur(value);
}

/**
 * Opens the Database management window
 */
void MainWindow::editColorDatabases() {
    ColorDatabaseManagement *window = new ColorDatabaseManagement(this);
    window->setWindowFlags(Qt::Window);
    window->setAttribute(Qt::WA_DeleteOnClose);
    window->show();
}

/**
 * Scales the image with a factor and draws a grid on it
 * @param scaleFactor
 */
void MainWindow::drawAndScaleImage(qreal scaleFactor) {
    QPixmap pixmap = QPixmap::fromImage(
        image.scaledToHeight(ui->label->pixmap()->height() * scaleFactor)
        );
    qreal scale = (qreal)pixmap.height()/(qreal)image.height();

    int px = ui->grid_x->text().toInt() * scale;
    int py = ui->grid_y->text().toInt() * scale;
    int w = ui->grid_width->text().toInt() * scale;
    int h = ui->grid_height->text().toInt() * scale;
    double cellSizeInMM = ui->grid_cellsize->text().toDouble(); //millimeters

    double origHeightMM = ui->orig_size_y->text().toDouble() * 10; //original size in mm
    double oneMMInPixels = (qreal)image.height() / origHeightMM;
    double cellSizeInPixels = cellSizeInMM * (oneMMInPixels * scale);

    QPainter *paint = new QPainter(&pixmap);
    paint->setPen(*(new QColor(255,34,255,255)));
    paint->drawRect(px, py, w, h);

    if(cellSizeInPixels != 0) {
        //rows
        for(double i = py; i<=py+h; i += cellSizeInPixels) {
            int yy = qRound(i);
            paint->drawLine(px, yy, px+w, yy);
        }

        //cols
        for(double i = px; i<=px+w; i += cellSizeInPixels) {
            int xx = qRound(i);
            paint->drawLine(xx, py, xx, py+h);
        }
    }

    paint->end();
    ui->label->setPixmap(pixmap);
    delete paint;
}

/**
 * Draws a grid to the image, and tells the
 * keyPressEvent to draw a grid
 */
void MainWindow::drawGrid() {
    drawAndScaleImage(1.0);
}
