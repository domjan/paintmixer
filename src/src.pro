QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../PaintMixer
CONFIG   += console
CONFIG   -= app_bundle

MOC_DIR =     ../build
UI_DIR =      ../build
RCC_DIR =     ../build
OBJECTS_DIR = ../build

TEMPLATE = app

SOURCES += *.cpp
HEADERS += *.h
FORMS += *.ui
