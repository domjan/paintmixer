#include "labcolorspace.h"
#include <math.h>

using namespace LabColorSpace;

/**
 * LabColor constructor
 * Converts the rgb value to L*a*b* space
 */
LabColor::LabColor(int red, int green, int blue)
{
    Triple Lab = XYZToLab( rgbToXYZ(Triple(red, green, blue)) );
    this->setLab(Lab.first, Lab.second, Lab.third);
}

/**
 * Calculates the difference fon an other color.
 * This method uses CIE94 algorithm
 * More info: http://en.wikipedia.org/wiki/Color_difference#CIE94
 * @param other an other Lab-scpace color
 */
double LabColor::difference(const LabColor &other) {
    double kL = 1.0, kC = 1.0, kH = 1.0, K1 = 0.045, K2 = 0.015;
    double deltaL = this->L() - other.L();
    double C1 = sqrt(pow(this->a(), 2)  + pow(this->b(), 2));
    double C2 = sqrt(pow(other.a(), 2)  + pow(other.b(), 2));
    double deltaCab = C1 - C2;

    double deltaA = this->a() - other.a();
    double deltaB = this->b() - other.b();
    double deltaHab = sqrt(deltaA*deltaA + deltaB*deltaB - deltaCab*deltaCab);
    double SL = 1.0, SC = 1+K1*C1, SH = 1+K2*C1;

    return sqrt(pow((deltaL / (kL*SL)), 2) + pow((deltaCab / (kC*SC)), 2) + pow((deltaHab / (kH*SH)), 2));
}

/**
 * Converts an [RGB] triplet to an XYZ triplet
 * @param rgb
 * @return XYZ triplet
 */
Triple LabColor::rgbToXYZ(const Triple &rgb)
{
    double temp_r, temp_g, temp_b;
    temp_r  = rgb.first / 255.0;
    temp_g  = rgb.second / 255.0;
    temp_b  = rgb.third / 255.0;

    if (temp_r > 0.04045) {
        temp_r  = pow(((temp_r + 0.055) / 1.055), 2.4);
    } else {
        temp_r /= 12.92;
    }

    if (temp_g > 0.04045) {
        temp_g  = pow(((temp_g + 0.055) / 1.055), 2.4);
    } else {
        temp_g /= 12.92;
    }

    if (temp_b > 0.04045) {
        temp_b  = pow(((temp_b + 0.055) / 1.055), 2.4);
    } else {
        temp_b /= 12.92;
    }

    temp_r  *= 100.0;
    temp_g  *= 100.0;
    temp_b  *= 100.0;

    double x = temp_r * 0.4124 + temp_g * 0.3576 + temp_b * 0.1805;
    double y = temp_r * 0.2126 + temp_g * 0.7152 + temp_b * 0.0722;
    double z = temp_r * 0.0193 + temp_g * 0.1192 + temp_b * 0.9505;
    return Triple(x, y, z);
}

/**
 * Converts an [XYZ] triplet to an Lab triplet
 * @param xyz
 * @return Lab triplet
 */
Triple LabColor::XYZToLab(const Triple &xyz)
{
    double x = xyz.first / refX;
    double y = xyz.second / refY;
    double z = xyz.third / refZ;

    if (x > 0.008856) {
        x = pow(x, 1.0/3.0);
    } else {
        x = (7.787 * x) + (16.0 / 116.0);
    }

    if (y > 0.008856) {
        y = pow(y, 1.0/3.0);
    } else {
        y = (7.787 * y) + (16.0 / 116.0);
    }

    if (z > 0.008856) {
        z = pow(z, 1.0/3.0);
    } else {
        z = (7.787 * z) + (16.0 / 116.0);
    }

    double L = (116.0 * y) - 16.0;
    double a = 500.0 * (x - y);
    double b = 200.0 * (y - z);
    return Triple(L, a, b);
}
