#include <colornamedatabase.h>
#include <QDebug>
#include <labcolorspace.h>
#include <QColor>
#include <QUrl>
#include <QFile>

using namespace LabColorSpace;

QDataStream &operator<<(QDataStream &out, const NamedColor &obj)
{
     out << obj.name << obj.r << obj.g << obj.b;
     return out;
}

QDataStream &operator>>(QDataStream &in, NamedColor &obj)
{
    in >> obj.name >> obj.r >> obj.g >> obj.b;
    return in;
}

/**
 * Constructor: opens a color database
 * Now it is static, knows two databases, but later it will be dynamic
 * User will create databases in the future
 * @param db
 */
ColorNameDatabase::ColorNameDatabase(const Database db) : dbName(""){
    switch(db) {
    case DATABASE_ALLCOLORS:
        dbName = "all_colors.txt";
        break;
    case DATABASE_ACRYLIC_COLORS:
        dbName = "acrylic_colors.txt";
        break;
    default:
        dbName = "";
    }

    if(dbName != "") {
        readDatabase(dbName);
    }
}

/**
 * Saves the currently opened database
 */
void ColorNameDatabase::save() {
    this->saveDatabase(dbName);
}

/**
 * Backup the database
 */
void ColorNameDatabase::backup() {
    QFile::copy(dbName, QString("%1.backup").arg(dbName));
}

/**
 * Load from backup file
 */
void ColorNameDatabase::loadFromBackup() {
    if(QFile::exists(QString("%1.backup").arg(dbName))) {
        readDatabase(QString("%1.backup").arg(dbName));
    }
}

/**
 * Writes the contents of the NamedColor vector to a database file
 * @param dbName file name
 */
void ColorNameDatabase::saveDatabase(const QString &dbName) {
    backup(); //backup the current database
    QFile file(dbName);
    file.open(QFile::WriteOnly);
    QDataStream out(&file);
    for(int i = 0; i<db.size(); i++) {
        out << db.at(i);
    }
    file.close();
}

/**
 * Reads a named color database
 * @param dbName file name
 */
void ColorNameDatabase::readDatabase(const QString &dbName) {
    db.clear(); //clear the current content
    QFile file(dbName);
    file.open(QFile::ReadOnly);
    QDataStream in(&file);
    while(!in.atEnd()) {
        NamedColor color;
        in >> color;
        db.append(color);
    }
    file.close();
}

/**
 * Find the nearest color in the opened database
 * @param r-g-b 0-255 value
 * @return the color
 */
NamedColor ColorNameDatabase::nearestColorName(int r, int g, int b) {
    LabColor color(r, g, b);
    int minIdx = -1;
    double minSum = INT32_MAX;
    for(int i = 0; i<db.size(); i++) {
        double sum = color.difference(LabColor(db[i].r, db[i].g, db[i].b));
        if(sum < minSum) {
            minIdx = i;
            minSum = sum;
        }
    }

    //did not find -> return empty color
    if(minIdx == -1) {
        NamedColor nc;
        return nc;
    }

    return db[minIdx];
}
