#ifndef COLORNAMEDATABASE_H
#define COLORNAMEDATABASE_H

#include <QString>
#include <QVector>

struct NamedColor {
    int r, g, b;
    QString name;
};

class ColorNameDatabase {
public:
    enum Database{
        DATABASE_ALLCOLORS = 0,
        DATABASE_ACRYLIC_COLORS,
        DATABASE_INVALID
    };

public:
    ColorNameDatabase(const Database db);
    NamedColor nearestColorName(int r, int g, int b);
    inline QVector<NamedColor> colors() { return db; }
    inline void setColors(QVector<NamedColor> &colors) { db = colors; }
    void save();
    void loadFromBackup();

private:
    void readDatabase(const QString &dbName);
    void saveDatabase(const QString &dbName);
    void backup();
    QVector<NamedColor> db;
    QString dbName;

    friend class TestColorNameDatabase;
};

#endif // COLORNAMEDATABASE_H
