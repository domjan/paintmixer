#include "colordatabasemanagement.h"
#include "ui_colordatabasemanagement.h"
#include <QColorDialog>

ColorDatabaseManagement::ColorDatabaseManagement(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ColorDatabaseManagement),
    db(NULL)
{
    ui->setupUi(this);
    openDatabase(ui->comboBox->currentIndex());
    this->setStyleSheet ( "QTableWidget::item:focus { border: 1px solid; }" );
}

/**
 * Opens a color database. All or Acrylic colors
 * @param currentIndex
 */
void ColorDatabaseManagement::ColorDatabaseManagement::openDatabase(int currentIndex) {
    delete db;
    db = new ColorNameDatabase((ColorNameDatabase::Database)currentIndex);
    QVector<NamedColor> colors = db->colors();
    fillColorsTableWidget( colors );
}

/**
 * Clears and fills the TableWidget with NamedColors
 * @param colors
 */
void ColorDatabaseManagement::fillColorsTableWidget(QVector<NamedColor> &colors) {
    while(ui->tableWidget->rowCount() > 0) ui->tableWidget->removeRow(0);
    foreach(NamedColor color, colors) {
        addRow(color.name, QColor(color.r, color.g, color.b));
    }
    ui->tableWidget->resizeColumnToContents(0);
}

/**
 * Edits the item in the table. First column: text editor,
 * second column: color picker
 * @param row item in the row
 * @param column item in the column
 */
void ColorDatabaseManagement::editColor(int row, int column) {
    if(column == 0) {
        ui->tableWidget->editItem(ui->tableWidget->item(row, column));
    }
    else {
        QColor color = QColorDialog::getColor(
                    ui->tableWidget->item(row, column)->backgroundColor());
        if(color.isValid()) {
            ui->tableWidget->item(row, column)->setBackgroundColor(color);
        }
    }
}


/**
 * Adds an empty row to the end of the list
 */
void ColorDatabaseManagement::addEmptyRow() {
    addRow(tr("New color"), QColor(Qt::white));
    ui->tableWidget->scrollToBottom();
}

/**
 * Adds a row to the end of the table
 * @param name
 * @param color
 */
void ColorDatabaseManagement::addRow(QString name, QColor color) {
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0,
                             new QTableWidgetItem(name));
    QTableWidgetItem *item = new QTableWidgetItem();
    item->setBackgroundColor(color);
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 1, item);
}

/**
 * Removes the currently selected row from the list
 */
void ColorDatabaseManagement::removeColor() {
    ui->tableWidget->removeRow(ui->tableWidget->currentRow());
}

/**
 * Adds all colors to the database and saves it
 */
void ColorDatabaseManagement::saveDatabaseAndClose() {
    QVector<NamedColor> colors;
    for(int i = 0; i<ui->tableWidget->rowCount(); i++) {
        NamedColor nc;
        nc.name = ui->tableWidget->item(i, 0)->text();
        QColor c = ui->tableWidget->item(i, 1)->backgroundColor();
        nc.r = c.red();
        nc.g = c.green();
        nc.b = c.blue();
        colors.append(nc);
    }
    db->setColors(colors);
    db->save();
    this->close();
}

/**
 * Load database from backup file (eg.: all_colors.txt.backup)
 */
void ColorDatabaseManagement::loadFromBackup() {
    db->loadFromBackup();
    QVector<NamedColor> colors = db->colors();
    fillColorsTableWidget( colors );
}

ColorDatabaseManagement::~ColorDatabaseManagement()
{
    delete ui;
}
