#ifndef GAUSSIANBLUR_H
#define GAUSSIANBLUR_H

#include <QColor>
#include <QRgb>
#include <QImage>

class GaussianBlur
{
public:
    GaussianBlur(int kernelSize = 0);
    GaussianBlur &operator=(const GaussianBlur &other);
    ~GaussianBlur();

    QRgb calculateOnePixel(const int &x, const int &y, const QImage *image);

private:
    void generateGaussianMatrix(int value);
    void freeMatrix();

private:
    double **gMatrix;
    int matrixSize;

    friend class TestGaussianBlur;
};

#endif // GAUSSIANBLUR_H
