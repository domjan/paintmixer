#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QImage>
#include <gaussianblur.h>
#include <colornamedatabase.h>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void keyPressEvent(QKeyEvent *);
    void closeEvent(QCloseEvent *event);

private slots:
    void openImage();
    void generateGaussianMatrix(int value);
    void editColorDatabases();
    void drawGrid();

private:
    void setInformationLabels(QPoint &pos);
    void drawAndScaleImage(qreal scaleFactor);
    QPoint getOriginalXYPixelPosition(QPoint pos);

    void loadInformationPage();
    void saveInformationPage();

private:
    Ui::MainWindow *ui;
    QImage image;
    GaussianBlur blur;

    ColorNameDatabase *allColorsDB;
    ColorNameDatabase *acrylicColorsDB;

    bool mouseRightButtonPressed;
    QString imageFileName;
};

#endif // MAINWINDOW_H
