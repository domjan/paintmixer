#include "columnchart.h"
#include <QPainter>

/**
 * Constructor: sets default parameters
 */
ColumnChart::ColumnChart(QWidget *parent) :
    QWidget(parent), verticalContentSpacing(10),
    horizontalContentSpacing(10), columnWidth(10), columnMaxHeight(100)
{
    setColumnCount(4);
    setColumnMaxHeight(100);
}

/**
 * Sets the color of the bars in the chart.
 */
void ColumnChart::setColumnColors(QVector<QColor> colors) {
    columnColors = colors;
    repaint();
}

/**
 * Sets the number of columns in the chart
 * @param columnCount number of bars. default = 4
 */
void ColumnChart::setColumnCount(int columnCount) {
    this->columnPercentages.clear();
    this->columnColors.clear();
    for(int i = 0; i<columnCount; i++) {
        this->columnPercentages.append(0.0);
        this->columnColors.append(Qt::black);
    }
    this->setFixedWidth(columnCount*columnWidth + (columnCount+1)*horizontalContentSpacing);
}

/**
 * Sets the max height of bars
 * @param maxHeight maximum height of bars. default = 100
 */
void ColumnChart::setColumnMaxHeight(int maxHeight) {
    this->setFixedHeight(maxHeight+2*verticalContentSpacing);
    columnMaxHeight = maxHeight;
    repaint();
}

/**
 * Set the height of a bar. The height will be a percentage of columnMaxHeight
 * @param columnIdx sets the ith column's height
 * @param percentage double value in range 0-1 (percentage/100)
 */
void ColumnChart::setColumnPercentage(int columnIdx, double percentage) {
    if(columnIdx < columnPercentages.size()
            && columnIdx >= 0
            && columnPercentages[columnIdx] != percentage) {
        this->columnPercentages[columnIdx] = percentage;
        repaint();
    }
}

/**
 * PaintEvent: paints bars
 * @param event
 */
void ColumnChart::paintEvent(QPaintEvent *event) {
    QPainter p(this);

    for(int i = 0; i<columnPercentages.size(); i++) {
        int barHeight = (columnPercentages[i] * (double)columnMaxHeight);

        if(columnColors.size() > i) {
            p.setPen(columnColors.at(i));
            p.setBrush(QBrush(columnColors.at(i)));
        }

        QRect bar(horizontalContentSpacing+i*(horizontalContentSpacing+columnWidth),
                  (verticalContentSpacing+columnMaxHeight-barHeight),
                  (columnWidth),
                  (barHeight));

        p.drawRect(bar);
    }

    p.end();
    QWidget::paintEvent(event);
}
