#ifndef COLORDATABASEMANAGEMENT_H
#define COLORDATABASEMANAGEMENT_H

#include <QWidget>
#include <colornamedatabase.h>

namespace Ui {
class ColorDatabaseManagement;
}

class ColorDatabaseManagement : public QWidget
{
    Q_OBJECT

public:
    explicit ColorDatabaseManagement(QWidget *parent = 0);
    ~ColorDatabaseManagement();

private slots:
    void openDatabase(int);
    void editColor(int,int);
    void addEmptyRow();
    void removeColor();
    void saveDatabaseAndClose();
    void loadFromBackup();

private:
    void fillColorsTableWidget(QVector<NamedColor> &colors);
    void addRow(QString name, QColor color);

private:
    Ui::ColorDatabaseManagement *ui;
    ColorNameDatabase *db;
};

#endif // COLORDATABASEMANAGEMENT_H
