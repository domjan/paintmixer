#include "gaussianblur.h"
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <QDebug>

const double pi = 3.14159265359;

/**
 * Constructor: generates a gaussian matrix
 * @param kernelSize
 */
GaussianBlur::GaussianBlur(int kernelSize) {
    gMatrix = NULL;
    generateGaussianMatrix(kernelSize);
}

/**
 * Assign operator
 */
GaussianBlur &GaussianBlur::operator=(const GaussianBlur &other) {
    freeMatrix();

    int value = other.matrixSize;
    gMatrix = new double*[value];
    matrixSize = value;

    //copy values
    for(int i = 0; i<value; i++) {
        gMatrix[i] = new double[value];
        for(int j = 0; j<value; j++) {
            gMatrix[i][j] = other.gMatrix[i][j];
        }
    }
    return *this;
}

/**
 * Destructor
 */
GaussianBlur::~GaussianBlur() {
    freeMatrix();
}

/**
 * Frees the memory allocated by the matrix
 */
void GaussianBlur::freeMatrix() {
    if(gMatrix != NULL) {
        for(int i = 0; i<matrixSize; i++) {
            delete[] gMatrix[i];
        }
        delete[] gMatrix;
        gMatrix = NULL;
    }
}

/**
 * Generates new Gaussian matrix, if the dimensions are
 * changed in the ui. Normalizes the matrix to 1.
 */
void GaussianBlur::generateGaussianMatrix(int value) {
    if(gMatrix == NULL || value != matrixSize) {
        freeMatrix();

        gMatrix = new double*[value];
        matrixSize = value;

        double sigma2 = 1; //sigma^2
        int center = matrixSize/2;
        double sum= 0.0;

        //calculate Gaussian values
        for(int i = 0; i<value; i++) {
            gMatrix[i] = new double[value];
            for(int j = 0; j<value; j++) {
                gMatrix[i][j] = (1.0/(2.0*pi*sigma2)) *
                        exp(-(( (pow(center-i, 2)+pow(center-j, 2)) / (2.0*sigma2)) ));
                sum += gMatrix[i][j] ;
            }
        }

        double ratio = 1.0/sum;

        //normalize to 1
        for(int i = 0; i<value; i++) {
            for(int j = 0; j<value; j++) {
                gMatrix[i][j] =  gMatrix[i][j] * ratio;
                //std::cout<<gMatrix[i][j]<<" ";
            }
            //std::cout<<std::endl;
        }
    }
}

/**
 * Calculate blurred value for one pixel
 * This is not optimal for using to blur the whole image.
 * @param x horizontal position
 * @param y vertical position
 * @param image the original image
 * @return the caclulated RGB value
 */
QRgb GaussianBlur::calculateOnePixel(const int &x, const int &y, const QImage *image) {
    //kernel matrix vertical and horizontal flip is unneccessary,
    //because it is symmetric
    double redSum = 0.0, greenSum = 0.0, blueSum = 0.0;
    int center = matrixSize/2;
    for(int i = -center; i<=center; i++) {
        for(int j = -center; j<=center; j++) {
            if(x+i >= 0 && y+j >= 0 && x+i < image->width() && y+j < image->height()) {
                QRgb p = image->pixel(x+i, y+j);
                redSum += qRed(p) * gMatrix[i+center][j+center];
                greenSum += qGreen(p) * gMatrix[i+center][j+center];
                blueSum += qBlue(p) * gMatrix[i+center][j+center];
            }
        }
    }
    return qRgb(redSum, greenSum, blueSum);
}
