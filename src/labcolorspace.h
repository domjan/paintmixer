#ifndef LABCOLORSPACE_H
#define LABCOLORSPACE_H

namespace LabColorSpace
{

//observer: 10 degrees, Illuminant = D65 (daylight)
//source: http://www.easyrgb.com/index.php?X=MATH&H=15#text15
const double refX = 94.811;
const double refY = 100.0;
const double refZ = 107.304;

class Triple {
public:
    double first, second, third;
    Triple(double f, double s, double t)
        : first(f), second(s), third(t)
    { }
};

class LabColor
{
public:
    LabColor(int red, int green, int blue);
    double difference(const LabColor &other);

    inline double L() const { return _L; }
    inline double a() const { return _a; }
    inline double b() const { return _b; }
    inline void setLab(double L, double a, double b) { _L = L; _a = a; _b = b;}

protected:
    Triple rgbToXYZ(const Triple &rgb);
    Triple XYZToLab(const Triple &xyz);

private:
    double _L, _a, _b;
};

}

#endif // LABCOLORSPACE_H
