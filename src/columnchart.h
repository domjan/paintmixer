#ifndef COLUMNCHART_H
#define COLUMNCHART_H

#include <QWidget>
#include <QVector>

class ColumnChart : public QWidget
{
    Q_OBJECT
public:
    explicit ColumnChart(QWidget *parent = 0);
    void setColumnPercentage(int columnIdx, double percentage);
    void setColumnMaxHeight(int maxHeight);
    void setColumnCount(int columnCount);
    void setColumnColors(QVector<QColor> colors);

protected:
    void paintEvent(QPaintEvent *);

private:
    int verticalContentSpacing;
    int horizontalContentSpacing;
    int columnWidth;
    int columnMaxHeight;

    QVector<double> columnPercentages;
    QVector<QColor> columnColors;

    friend class TestColumnChart;
};

#endif // COLUMNCHART_H
