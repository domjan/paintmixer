#include "testgaussianblur.h"
#include <testhelpers.h>
#include <gaussianblur.h>


/*
  0.07511 0.1283 0.07511
  0.1238  0.2041 0.1238
  0.07511 0.1283 0.07511
 */
void TestGaussianBlur::testGaussianMatrix() {
    GaussianBlur g(1);
    QCOMPARE(g.gMatrix[0][0], 1.0);

    GaussianBlur g3(3);
    QVERIFY(doubleCompare(g3.gMatrix[0][0], 0.07511, 4));
    QVERIFY(doubleCompare(g3.gMatrix[0][1], 0.1238, 4));
    QVERIFY(doubleCompare(g3.gMatrix[0][2], 0.07511, 4));
    QVERIFY(doubleCompare(g3.gMatrix[1][0], 0.1238, 4));
    QVERIFY(doubleCompare(g3.gMatrix[1][1], 0.2041, 4));
    QVERIFY(doubleCompare(g3.gMatrix[1][2], 0.1238, 4));
    QVERIFY(doubleCompare(g3.gMatrix[2][0], 0.07511, 4));
    QVERIFY(doubleCompare(g3.gMatrix[2][1], 0.1238, 4));
    QVERIFY(doubleCompare(g3.gMatrix[2][2], 0.07511, 4));
}

void TestGaussianBlur::testCalculateForOnePixel() {
    QImage image(4, 4, QImage::Format_RGB32);
    //first row
    image.setPixel(0, 0, qRgb(10, 34, 12));
    image.setPixel(0, 1, qRgb(2,2,2));
    image.setPixel(0, 2, qRgb(4,4,4));
    image.setPixel(0, 3, qRgb(7,7,7));

    //second row
    image.setPixel(1, 0, qRgb(2,3,3));
    image.setPixel(1, 1, qRgb(12,34,56));
    image.setPixel(1, 2, qRgb(5,67,2));
    image.setPixel(1, 3, qRgb(1,4,6));

    //third row
    image.setPixel(2, 0, qRgb(0, 34, 10));
    image.setPixel(2, 1, qRgb(5,2,6));
    image.setPixel(2, 2, qRgb(4,2,8));
    image.setPixel(2, 3, qRgb(1,78,3));

    //forth row
    image.setPixel(3, 0, qRgb(10, 4, 1));
    image.setPixel(3, 1, qRgb(1,5,67));
    image.setPixel(3, 2, qRgb(9,1,3));
    image.setPixel(3, 3, qRgb(3,3,3));


    GaussianBlur g3(3);
    double sum = 0.2041*10 + 0.1238*2 + 0.1283*2 + 0.07511*12;
    QCOMPARE(qRed((g3.calculateOnePixel(0, 0, &image))), (int)sum);

    sum = 0.07511*34.0 + 0.1283*2.0 + 0.07511*4
            + 0.1283*3.0 + 0.2041*34.0 + 0.1283*67.0
            + 0.07511*34.0 + 0.1283*2 + 0.07511*2.0;
    QCOMPARE(qGreen((g3.calculateOnePixel(1, 1, &image))), (int)sum);

    sum = 0.07511*8.0 + 0.1283*3.0
            + 0.1283*3.0 + 0.2041*3.0;
    QCOMPARE(qBlue((g3.calculateOnePixel(3, 3, &image))), (int)sum);
}
