#include "testpaintmixer.h"

#include <testcolumnchart.h>
#include <testlabcolorspace.h>
#include <testgaussianblur.h>
#include <testcolornamedatabase.h>

int main(int argc, char **argv) {
    QApplication a(argc, argv);
    TestColumnChart t1;
    TestLabColorSpace t2;
    TestGaussianBlur t3;
    TestColorNameDatabase t4;

    QTest::qExec(&t1);
    QTest::qExec(&t2);
    QTest::qExec(&t3);
    QTest::qExec(&t4);
}
