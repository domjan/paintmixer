QT       += core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PaintMixerTest
CONFIG   += console
CONFIG   -= app_bundle

MOC_DIR =     ../build
UI_DIR =      ../build
RCC_DIR =     ../build
OBJECTS_DIR = ../build

TEMPLATE = app

LIBS += ../build/labcolorspace.o ../build/columnchart.o ../build/moc_columnchart.o \
        ../build/gaussianblur.o ../build/colornamedatabase.o

INCLUDEPATH += ../src

SOURCES += *.cpp
HEADERS += *.h
