#include "testcolornamedatabase.h"
#include <colornamedatabase.h>
#include <QTemporaryFile>

void TestColorNameDatabase::testSaveAndReadDatabase() {
    QTemporaryFile file;
    file.open();
    file.write(" ");
    file.close();
    ColorNameDatabase database(ColorNameDatabase::DATABASE_INVALID);
    database.db.clear();
    NamedColor n1;
    n1.name = "First";
    n1.r = 11;
    n1.g = 111;
    n1.b = 1;
    NamedColor n2;
    n2.name = "Second";
    n2.r = 22;
    n2.g = 222;
    n2.b = 2;
    database.db.append(n1);
    database.db.append(n2);
    database.saveDatabase(file.fileName());

    ColorNameDatabase readed(ColorNameDatabase::DATABASE_INVALID);
    readed.readDatabase(file.fileName());

    QCOMPARE(readed.db.size(), 2);
    QCOMPARE(readed.db[0].name, QString("First"));
    QCOMPARE(readed.db[1].name, QString("Second"));

    QCOMPARE(readed.db[0].r, 11);
    QCOMPARE(readed.db[1].r, 22);

    QCOMPARE(readed.db[0].g, 111);
    QCOMPARE(readed.db[1].g, 222);

    QCOMPARE(readed.db[0].b, 1);
    QCOMPARE(readed.db[1].b, 2);

}

void TestColorNameDatabase::testFindNearestColor() {
    ColorNameDatabase database(ColorNameDatabase::DATABASE_INVALID);
    database.db.clear();
    NamedColor nc = database.nearestColorName(127, 127, 127);
    QCOMPARE(nc.name, QString(""));
    NamedColor n1;
    n1.name = "First";
    n1.r = 11;
    n1.g = 111;
    n1.b = 1;
    database.db.append(n1);

    nc = database.nearestColorName(127, 127, 127);
    QCOMPARE(nc.name, QString("First"));

    NamedColor n2;
    n2.name = "Second";
    n2.r = 100;
    n2.g = 100;
    n2.b = 100;
    database.db.append(n2);
    nc = database.nearestColorName(127, 127, 127);
    QCOMPARE(nc.name, QString("Second"));
}
