#ifndef TESTCOLUMNCHART_H
#define TESTCOLUMNCHART_H

#include <QtTest/QtTest>

class TestColumnChart : public QObject
{
    Q_OBJECT

private slots:
    void testColumnPercentage();
    void testColumnMaxHeight();
    void testColumnColumnCount();
    void testColumnColors();
};

#endif // TESTCOLUMNCHART_H
