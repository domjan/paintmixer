#ifndef TESTGAUSSIANBLUR_H
#define TESTGAUSSIANBLUR_H

#include <QtTest/QtTest>

class TestGaussianBlur : public QObject
{
    Q_OBJECT

private slots:
    void testGaussianMatrix();
    void testCalculateForOnePixel();
};

#endif // TESTGAUSSIANBLUR_H
