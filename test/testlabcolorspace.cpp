#include "testlabcolorspace.h"
#include <labcolorspace.h>
#include <testhelpers.h>

using namespace LabColorSpace;

class LabColorMock : public LabColor
{
public:
    LabColorMock(int r, int g, int b) : LabColor(r, g, b) { }
    Triple rgbToXYZ(const Triple &rgb) {
        return LabColor::rgbToXYZ(rgb);
    }
    Triple xyzToLab(const Triple &xyz) {
        return LabColor::XYZToLab(xyz);
    }
};

void TestLabColorSpace::testRgbToXYZ()
{
    //black
    LabColorMock color(0, 0, 0);
    Triple xyz1 = color.rgbToXYZ(Triple(0, 0, 0));
    QCOMPARE(xyz1.first, 0.0);
    QCOMPARE(xyz1.second, 0.0);
    QCOMPARE(xyz1.third, 0.0);

    //white
    Triple xyz2 = color.rgbToXYZ(Triple(255, 255, 255));
    QCOMPARE(xyz2.first, 95.05);
    QCOMPARE(xyz2.second, 100.0);
    QCOMPARE(xyz2.third, 108.9);

    //red
    Triple xyz3 = color.rgbToXYZ(Triple(255, 0, 0));
    QCOMPARE(xyz3.first, 41.24);
    QCOMPARE(xyz3.second, 21.26);
    QCOMPARE(xyz3.third, 1.93);

    //gray
    Triple xyz4 = color.rgbToXYZ(Triple(127, 127, 127));
    QVERIFY(doubleCompare(xyz4.first, 20.1725, 3));
    QVERIFY(doubleCompare(xyz4.second, 21.223, 3));
    QVERIFY(doubleCompare(xyz4.third, 23.1119, 3));
}

void TestLabColorSpace::testXyzToLab()
{
    //black
    LabColorMock color(0, 0, 0);
    Triple xyz1 = color.xyzToLab(Triple(0, 0, 0));
    QCOMPARE(xyz1.first, 0.0);
    QCOMPARE(xyz1.second, 0.0);
    QCOMPARE(xyz1.third, 0.0);

    //red
    Triple xyz2 = color.xyzToLab(Triple(41.240, 21.260, 1.930));
    QVERIFY(doubleCompare(xyz2.first, 53.232, 3));
    QVERIFY(doubleCompare(xyz2.second, 80.423, 3));
    QVERIFY(doubleCompare(xyz2.third, 66.965, 3));

    //gray
    Triple xyz3 = color.xyzToLab(Triple(20.173, 21.223, 23.112));
    QVERIFY(doubleCompare(xyz3.first, 53.192, 3));
    QVERIFY(doubleCompare(xyz3.second, 0.253, 3));
    QVERIFY(doubleCompare(xyz3.third, -0.588, 3));
}
