#include "testhelpers.h"
#include <math.h>
#include <QDebug>

bool doubleCompare(double first, double second, int precision) {
    //qDebug()<<"Actual: "<<(int)(first*pow(10, precision))<<" Expected: "<<(int)(second*pow(10, precision));
    return (int)(first*pow(10, precision)) == (int)(second*pow(10, precision));
}
