#ifndef TESTHELPERS_H
#define TESTHELPERS_H

bool doubleCompare(double first, double second, int precision);

#endif // TESTHELPERS_H
