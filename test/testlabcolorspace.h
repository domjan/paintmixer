#ifndef TESTLABCOLORSPACE_H
#define TESTLABCOLORSPACE_H

#include <QtTest/QtTest>

class TestLabColorSpace : public QObject
{
    Q_OBJECT

private slots:
    void testRgbToXYZ();
    void testXyzToLab();
};

#endif // TESTLABCOLORSPACE_H
