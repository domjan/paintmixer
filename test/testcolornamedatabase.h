#ifndef TESTCOLORNAMEDATABASE_H
#define TESTCOLORNAMEDATABASE_H

#include <QtTest/QtTest>

class TestColorNameDatabase : public QObject
{
    Q_OBJECT

private slots:
    void testSaveAndReadDatabase();
    void testFindNearestColor();
};

#endif // TESTCOLORNAMEDATABASE_H
