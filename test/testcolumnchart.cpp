#include "testcolumnchart.h"
#include <columnchart.h>

void TestColumnChart::testColumnPercentage() {
    ColumnChart chart(0);
    chart.columnPercentages.clear();
    chart.columnPercentages.append(0.0);
    chart.columnPercentages.append(0.0);
    chart.setColumnPercentage(0, 2.4);
    QCOMPARE(chart.columnPercentages[0], 2.4);
    QCOMPARE(chart.columnPercentages[1], 0.0);

    chart.setColumnPercentage(1, 5.8);
    QCOMPARE(chart.columnPercentages[0], 2.4);
    QCOMPARE(chart.columnPercentages[1], 5.8);

    chart.setColumnPercentage(2, 5.8); //invalid idx
    QCOMPARE(chart.columnPercentages[0], 2.4);
    QCOMPARE(chart.columnPercentages[1], 5.8);

    chart.setColumnPercentage(-1, 5.8); //invalid idx
    QCOMPARE(chart.columnPercentages[0], 2.4);
    QCOMPARE(chart.columnPercentages[1], 5.8);

    QCOMPARE(chart.columnPercentages.size(), 2);
}

void TestColumnChart::testColumnMaxHeight() {
    ColumnChart chart(0);
    chart.verticalContentSpacing = 30;
    chart.setColumnMaxHeight(140);
    QCOMPARE(chart.columnMaxHeight, 140);
    QCOMPARE(chart.height(), 30*2 + 140);

    chart.setColumnMaxHeight(150);
    QCOMPARE(chart.columnMaxHeight, 150);
    QCOMPARE(chart.height(), 30*2 + 150);
}

void TestColumnChart::testColumnColumnCount() {
    ColumnChart chart(0);
    chart.columnPercentages.clear();
    chart.setColumnCount(7);
    QCOMPARE(chart.columnPercentages.size(), 7);

    chart.columnWidth = 30;
    chart.horizontalContentSpacing = 40;
    chart.setColumnCount(5);
    QCOMPARE(chart.columnPercentages.size(), 5);
    QCOMPARE(chart.width(), 5*(30+40) + 40);
    QCOMPARE(chart.columnColors.size(), 5);
}

void TestColumnChart::testColumnColors() {
    ColumnChart chart(0);

    QVector<QColor> colors;
    colors << Qt::cyan << Qt::magenta;
    chart.setColumnColors(colors);
    QCOMPARE(chart.columnColors.size(), 2);
    QCOMPARE(chart.columnColors.at(0), QColor(Qt::cyan));
    QCOMPARE(chart.columnColors.at(1), QColor(Qt::magenta));
}
